#必要なモジュールをインポート
import os
# import sys 
import glob
from pathlib import Path
import urllib.parse

from flask import (
    Flask, 
    request, 
    redirect, 
    url_for, 
    make_response, 
    jsonify, 
    render_template, 
    send_from_directory)
from numpy.lib.type_check import imag
import tensorflow.compat.v1 as tf
#顔認識用のvggモデル
#from keras_vggface import vggface
#vggface.pyをvggという名前でインポート
from scipy.spatial.distance import cosine
#画像の前処理に用いるモジュール
from tensorflow.python.keras.models import load_model
from tensorflow.keras.preprocessing import image
from keras.applications.imagenet_utils import preprocess_input
import numpy as np


from tensorflow.python.keras.backend import set_session

# csv
import urllib.request, json
import urllib.parse
import datetime

#Google Maps Platform Directions API endpoint
endpoint = 'https://maps.googleapis.com/maps/api/directions/json?'
api_key = 'your API key'


# 目的地の緯度経度取得
import pandas as pd
csv_input = pd.read_csv(filepath_or_buffer="./static/spot.csv", encoding="utf-8", sep=",")



def get_similarity(face_vector1, face_vector2):
    return 1 - cosine(face_vector1, face_vector2)

# 画像のアップロード先のディレクトリ
UPLOAD_FOLDER_ENTER = './image_user' #芸能人の写真用
UPLOAD_FOLDER_USER = './image_user' #ユーザーの写真用
FROM_PATH_TO_VECTOR = {} #画像パスとベクトルを紐つけるための空の辞書

#FlaskでAPIを書くときのおまじない
app = Flask(__name__,
static_folder='./static')


glob_dir = os.path.join("./static/画像", "*.jpg")
files = glob.glob(glob_dir)


from tensorflow.python.keras.backend import set_session
from tensorflow.python.keras.models import load_model





#  tf_config = some_custom_config
#  sess = tf.Session(config=tf_config)
sess = tf.Session()
graph = tf.get_default_graph()
set_session(sess)
# vggのfc2までの層をload
model = load_model('fc2')

import annoy
from annoy import AnnoyIndex
dim = 4096
annoy_model = AnnoyIndex(dim)

import pandas as pd
csv_input = pd.read_csv(filepath_or_buffer="./static/spot.csv", encoding="utf-8", sep=",")





#アプリのホーム画面のhtmlを返すURL
@app.route('/')
def index():
    return render_template(
        'index.html'
          )


@app.route('/upload', methods=['GET', 'POST'])
def upload_files():
    # リクエストがポストかどうかの判別
    if request.method == 'GET':
            return render_template(
        'image.html', user_images=os.listdir(UPLOAD_FOLDER_USER)[::-1])
        
    else:
        #  フォルダ内に画像があるならjpgを抽出して削除
           file_list=glob.glob("./image_user/*.jpg")
           if len(file_list)!=0:
              for i in range(len(file_list)):
                  os.remove(file_list[i])
        #  入力upload_fileをPLOAD_FOLDER_USERに保存
           upload_file = request.files['upload_file']
           img_path = os.path.join(UPLOAD_FOLDER_USER,upload_file.filename)
           upload_file.save(img_path)



           global sess
           global graph
           with graph.as_default():
             set_session(sess)

             img = image.load_img(img_path, target_size=(224, 224))
             x = image.img_to_array(img)
             x = np.expand_dims(x, axis=0)
             x = preprocess_input(x)
             fc2_features = model.predict(x)
            # print('len: ', len(fc2_features[0]))
         

            

             from annoy import AnnoyIndex
             trained_model = AnnoyIndex(4096)
             trained_model.load("../kokotabi/result.ann")

# インデックス0付近の10000個のデータを返す。全データがこの値より小さいときは実データ数になるっぽい
#  print(trained_model.get_nns_by_item(0, 10000))
# 第一引数に検索したいインデックス番号を指定。第二引数は得る個数。今回は7個getしよう。
             import pandas as pd
             #df= pd.read_csv("../kokotabi/result.ann", sep='^([^\s]*)\s', engine='python', header=None).drop(0, axis=1)
             #df.head()

          
             items = trained_model.get_nns_by_vector(fc2_features[0], 5, search_k=-1, include_distances=False)
             print(items)
             return render_template(
               'image.html', user_images=os.listdir(UPLOAD_FOLDER_USER)[::-1], items=items, files=files)
             #  画像表示


@app.route('/route', methods=['GET', 'POST'])
def route():
    if request.method == 'GET':
        #  クリックした画像のインデックスを取得
        index = request.args.get('index')
        if index==None:
            return render_template('route.html')
        # ファイル名を取得
        name_jpg = os.path.splitext(os.path.basename(files[int(index)]))[0]
        latitude = csv_input.values[csv_input["name"] == int(name_jpg), 2]
        longitude = csv_input.values[csv_input["name"] == int(name_jpg), 3]
        #print(latitude)
        destination = str(latitude[0]) +' '+ str(longitude[0])
        #print(index)
        #print(files[int(index)])
        #print(name_jpg)
        return render_template('route.html', name_jpg=name_jpg, destination=destination)
     

    else:
        origin = request.form.get('origin')
        destination = request.form.get('destination')
        if origin == '' and destination == '':
            disttans='None'
            duration='None'
            origin='None'
            destination='None'
        elif origin == '':
             disttans='None'
             duration='None'
             origin='None'
             #destination='None'
        elif destination == '':
             disttans='None'
             duration='None'
             #origin='None'
             destination='None'

        else:

            nav_request = 'language=ja&origin={}&destination={}&key={}'.format(origin,destination,api_key)
            nav_request = urllib.parse.quote_plus(nav_request, safe='=&')
            requesting = endpoint + nav_request
            # print(requesting)
          

            #Google Maps Platform Directions APIを実行
            response = urllib.request.urlopen(requesting).read()
  

            #結果(JSON)を取得
            directions = json.loads(response)
            #所要時間を取得
            if directions['routes']==[]:
             # print('No')
             disttans='None'
             duration='No'
             #origin='No'
             #destination='No'

            for key in directions['routes']:
                #print(key) # titleのみ参照
                #print(key['legs']) 
                for key2 in key['legs']:
                   # print('')
                   # print('=====')
                    disttans=key2['distance']['text']
                    duration=key2['duration']['text']
                   # print(key2['distance']['text'])
                   # print(key2['duration']['text'])
                   # print('=====')

        
        
        
        return render_template(
        'route.html', disttans=disttans,duration=duration,origin=origin, destination=destination)
    


#ディレクトリに保存されている画像をブラウザに送る処理
@app.route('/images/<path:path>')
def send_image(path):
    return send_from_directory(UPLOAD_FOLDER_USER, path)

@app.context_processor
def override_url_for():
    return dict(url_for=dated_url_for)

def dated_url_for(endpoint, **values):
    if endpoint == 'static':
        filename = values.get('filename', None)
        if filename:
            file_path = os.path.join(app.root_path,
                                     endpoint, filename)
            values['q'] = int(os.stat(file_path).st_mtime)
    return url_for(endpoint, **values)

#スクリプトからAPIを叩けるようにします。
if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000)